<!DOCTYPE html>
<html>
  <head>
    <!-- Thanks to @Vandesm14 and Repl.it for the original template (@templates/Personal-Blog-Site) -->
    <!-- Feed created using Feedlang -->
    <!-- Feedlang was created by Lucas M.T. in 2020 -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width">
    <link href="./style.css" rel="stylesheet" type="text/css" />
    <link rel="icon" href="./img/favicon.png" type="image/x-icon" />
  </head>
  <body>
    <main>
     <?php include "./feed.flwf" ?>
    </main>
  </body>
  <footer>
    <article>
      <p>Coded by <a href="https://lucas64.tk">Lucas</a>, created with <a href="https://repl.it/@templates/Personal-Blog-Site">Personal Blog Site</a>. Site finished the 14-8-2020</p>
      <p>(All the dates in this website are DD/MM/YYYY)</p>
    </article>
  </footer>
  <script>
    console.log("🥚");
    console.log("OW! You find an easter egg!!!");
    console.log(":D");
  </script>
</html>