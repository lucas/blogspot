# A real blog with Feedlang
## Made with HTML, CSS, PHP & Feedlang
(coming soon an explanation of what is Feedlang)
## Help
To create a post you have to copy and paste "p/lorem-ipsum" and for publish it in the principal page, you have to modify the Feedlang file (feed.flwf) and put another item (aka article).
## Requirements
-PHP
## Code available in:
[Repl.it](https://repl.it/@L64/blogspot) - [GitHub](https://github.com/L64/blogspot)
## Thanks
Thanks to @Vandesm14 and Repl.it for the original template (@templates/Personal-Blog-Site)
### [DEMO](https://blogspot.l64.repl.co) - [![Run on Repl.it](https://repl.it/badge/github/L64/blogspot)](https://repl.it/github/L64/blogspot)
