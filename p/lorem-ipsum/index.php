<!DOCTYPE html>
<html id="top">
  <head>
      <meta charset="utf-8">
      <title><?php include "text/Title.txt" ?> by <?php include "text/Author.txt" ?></title>
      <link rel="icon" href="/img/favicon.png">
    <link rel="stylesheet" type="text/css" href="/style.css">
  </head>
  <body>
    <h3 class="pubBack"><a href="/">⬅ Back</a></h3>
    <article>
      <h1 id="title"><?php include "text/Title.txt" ?></h1>
      <p>by <a href="<?php include "text/Author Webpage.txt" ?>"><?php include "text/Author.txt" ?></a> <date><?php include "text/Date.txt" ?></date></p>
      <hr />
      <p><?php include "text/Story.txt" ?></p>
    </article>
    <footer>
      <article>
        <h1 class="downtoup_arrow"><a href="#top">⬆</a></h1>
        <p>Coded by <a href="https://lucas64.tk">Lucas</a>, created with <a href="https://repl.it/@templates/Personal-Blog-Site">Personal Blog Site</a>. Site finished the 14-8-2020. (All the dates in this website are DD/MM/YYYY)</p>
      </article>
    </footer>
  </body>
    <!-- Based on Change-the-Text -->
    <!-- Created by Lucas M.T. in 2020 -->
</html>